import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { CLOSE_MODAL, OPEN_INFO_DESCRIPTION, OPEN_RESULT_DESCRIPTION } from '../tools/actions';

import { Link } from 'react-router-dom';

import { addSpaceSeparator, paragraphFormatter } from '../tools/helpers';

import DoctorBag from 'assets/doctorbag.svg';
import BrokenArm from 'assets/broken-arm.svg';
import Child from 'assets/Bear.svg';

import Modal from 'Modal';
import { lossRange } from '../tools/recommendedValue';

const Panel = ({ image, main, className, children, infoLink }) => {
  const classes = className ? `insurance-panel ${className}` : 'insurance-panel';
  return (
    <div className={classes}>
      <div className="panel-wrapper">
        {image}
        <div className="main">
          {main}
          {children}
        </div>
        {infoLink}
      </div>
    </div>
  );
};
const Result = ({ lossValue, texts, openDescription, lowestSalary, highestSalary }) =>
  (
    <Fragment>
      <div className="insurance-panel info">
        <div className="panel-wrapper result">
          <DoctorBag className="image"/>
          <div className="main">
            {lossValue === lowestSalary ? paragraphFormatter(texts.lossOnMinIncome) :
              lossValue === highestSalary ? paragraphFormatter(texts.lossOnMaxIncome) :
                paragraphFormatter(texts.main, [addSpaceSeparator(lossValue),addSpaceSeparator(lossValue*12)],'big')}
          </div>
        </div>
        <button
          className="description-link"
          id="your-value-calculations-description"
          onClick={openDescription}
        >
          {texts.calculationDescriptionLink}
        </button>
      </div>
    </Fragment>
  );

class Info extends Component {
  openResultModal = () => {
    this.props.dispatch({ type: OPEN_RESULT_DESCRIPTION });
  };

  openInfoModal = () => {
    this.props.dispatch({ type: OPEN_INFO_DESCRIPTION });
  };

  closeModal = () => {
    this.props.dispatch({ type: CLOSE_MODAL });
  };

  getModalData = () => {
    const { texts, showResultDescription, showInfoDescription } = this.props;
    const showModal = showResultDescription || showInfoDescription;
    const modalTitle = showResultDescription
      ? texts.calculationDescriptionTitle
      : texts.infoDescriptionTitle;
    const modalText = showResultDescription
      ? texts.calculationDescriptionText
      : texts.infoDescriptionText;

    return {
      showModal,
      modalTitle,
      modalText
    };
  };

  infoLink = (
    <button
      className="description-link"
      id="disability-probability-description"
      onClick={this.openInfoModal}
    >
      {this.props.texts && this.props.texts.infoDescriptionLink}
    </button>
  );

  trackAndNavigateToSSX = e => {
    e.preventDefault();

    const { salary } = this.props;
    const parsedSalary = parseInt(salary.replace(/ /g, ''));
    const _value = lossRange.reduce((acc, v) => v.revenue > parsedSalary ? acc : v).value;
    const recommendedValue = (!_value || parsedSalary <= 300000 || parsedSalary >= 1000000) ? undefined : _value;


    window.sessionStorage.setItem('form-data-prefill', btoa(JSON.stringify({
      salary,
      recommendedValue
    })));
  };

  render() {
    const { texts } = this.props;
    const { modalTitle, modalText, showModal } = this.getModalData();
    return (
      <Fragment>
        <Result {...this.props} openDescription={this.openResultModal}/>
        <Panel
          className="insurance-panel info"
          image={<Child className="image"/>}
          main={paragraphFormatter(texts.child)}
        />
        <Panel
          className="insurance-panel info last"
          image={<BrokenArm className="image"/>}
          main={paragraphFormatter(texts.bandaidHead)}
          infoLink={this.infoLink}
        />
        <Modal
          isOpen={showModal}
          closeHandler={this.closeModal}
          modalLabel={modalTitle}
          modalId="info-modal"
          title={modalTitle}
        >
          <p className="description">{modalText}</p>
        </Modal>
        <a
          onClick={this.trackAndNavigateToSSX}
          href={`${window.location.origin}/sjekk-pris/#/UforeFlex/info`}
          className="insurance-btn floating"
          id="check-disability-insurance-price"
        >
          {texts.fwdButton}
        </a>
        <Link to="/salary" className="navigate-back">
          {texts.backButton}
        </Link>
      </Fragment>
    );
  }
}

const { string, oneOfType, func, objectOf, bool, node, arrayOf, number } = PropTypes;

Panel.propTypes = {
  children: oneOfType([node, arrayOf(node)]),
  className: string,
  image: node,
  infoLink: node,
  main: node
};

Result.propTypes = {
  lossValue: number,
  openDescription: func,
  texts: objectOf(string),
  lowestSalary: number,
  highestSalary: number,
};

Info.propTypes = {
  texts: objectOf(string),
  lossValue: number,
  salary: string,
  showResultDescription: bool,
  showInfoDescription: bool,
  dispatch: func
};

export default connect(
  ({
    texts,
    salary,
    lowestSalary,
    highestSalary,
    showResultDescription,
    showInfoDescription,
    lossValue
  }) => ({
    lossValue,
    salary,
    lowestSalary,
    highestSalary,
    showResultDescription,
    showInfoDescription,
    texts: texts.results
  })
)(Info);
export { Panel };
