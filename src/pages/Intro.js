import React, { Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { TextField,RadioGroup,Radio } from '@material-ui/core/';
import { FormControlLabel } from '@material-ui/core';
import texts from '../tools/texts';

const IntroPage = ()=> (
  <div>
    <div>
    </div>
    <Fragment>
      <div className="insurance-panel">
        <TextField
          id='salary'
          label='Din årslønn'
          defaultValue={560000}
          margin='normal'
        />

        <RadioGroup aria-label='Sivilstatus'>
          <FormControlLabel control={<Radio color='primary'/>} label='Gift' value='Gift' />
          <FormControlLabel control={<Radio color='primary'/>} label='Samboer' value='Samboer'/>
          <FormControlLabel control={<Radio color='primary'/>} label='Enslig'/>
        </RadioGroup>

        <TextField
          id='partnerfødtår'
          label='Hvilket år er partneren din født?'
          defaultValue={1989}
        />
      </div>
      <button
        className="insurance-btn"
        onClick={() => handleSubmit()}
      >
        {texts.fwdButton}
      </button>
    </Fragment>
  </div>
);

const handleSubmit =()=>{
  //browserHistory.push('/login');
  console.log('handle submit');
  return  <Redirect push to="/Salary" />;
};

/*
const ResultPage = () =>(
  <div>Next Page loaded..</div>
);

const NotFoundPage=()=>(<div>Error, request not found</div>);
*/

class Intro extends React.Component{
  state={ havePartner: false };
  render() {
    return (
      <div>
        <IntroPage {...this.props} havePartner={this.state} />
      </div>
    );
  }
}


const { func, object, shape, string } = PropTypes;
IntroPage.propTypes = {
  dispatch: func,
  history: object,
  texts: shape({
    main: string,
    info: string,
    fwdButton: string
  })
};

export default Intro;
