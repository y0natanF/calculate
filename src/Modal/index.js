import React from 'react';
import PropTypes from 'prop-types';

import { CSSTransitionGroup } from 'react-transition-group';

import './Modal.css';

const blockClickEvent = event => {
  event.stopPropagation();
};

const notPropagatingHandler = handler => event => {
  blockClickEvent(event);
  handler(event);
};

const noOp = () => null;

const disableBodyScroll = () => window.$('body').addClass('no-scroll');
const enableBodyScroll = () => window.$('body').removeClass('no-scroll');

class Backdrop extends React.Component {
  componentDidMount() {
    disableBodyScroll();
  }

  componentWillUnmount() {
    enableBodyScroll();
  }

  render() {
    return <div className="modal-backdrop"/>;
  }
}

const Modal = ({
  children,
  isOpen,
  modalId,
  modalLabel,
  closeHandler,
  title,
  subtitle
}) => {
  const showHeader = !!title || !!subtitle;
  // if closeHandler is not passed as prop, the modal is 'not closable'
  // the children components are responsible to dispatch the event which close it
  const closeFn = closeHandler || noOp;
  const notPropagatingCloseHandler = notPropagatingHandler(closeFn);
  const onEnterOrSpace = e => {
    if (e.key === 'Enter' || e.key === ' ') {
      notPropagatingCloseHandler(e);
    }
  };
  const modalBox = (
    <div className="insurance-modal">
      <CSSTransitionGroup
        transitionName="fade"
        transitionEnterTimeout={300}
        transitionLeaveTimeout={300}>
        {isOpen && <Backdrop/>}
      </CSSTransitionGroup>
      <CSSTransitionGroup
        transitionName="slide"
        transitionEnterTimeout={300}
        transitionLeaveTimeout={300}>
        {isOpen && (
          <div
            onClick={notPropagatingCloseHandler}
            className="modal in"
            id={modalId}
            tabIndex="-1"
            role="dialog"
            aria-labelledby={modalLabel}
            style={{ display: 'block' }}
          >
            <div className="modal-dialog" role="document">
              <div className="modal-content" onClick={blockClickEvent}>
                {closeHandler && (
                  <span
                    onClick={notPropagatingCloseHandler}
                    onKeyPress={onEnterOrSpace}
                    role="button"
                    aria-hidden="true"
                    className="insurance-close-btn"
                    tabIndex="0"
                  >
                    <i className="remove "/>
                  </span>
                )}
                {showHeader && (
                  <div className="modal-header padding-top-25">
                    {title && (
                      <h2
                        className={`modal-title h1lookalike insurance-header${
                          closeHandler ? ' width-90' : ''
                        }`}
                      >
                        {title}
                      </h2>
                    )}
                    {subtitle && (
                      <p className="modal-title insurance-description">{subtitle}</p>
                    )}
                  </div>
                )}
                <div
                  className={`modal-body${showHeader ? '' : ' padding-top-25'}`}
                >
                  {children || null}
                </div>
              </div>
            </div>
          </div>
        )}
      </CSSTransitionGroup>
    </div>
  );

  return modalBox;
};

const { bool, string, func } = PropTypes;

Modal.propTypes = {
  closeHandler: func,
  isOpen: bool.isRequired,
  modalId: string.isRequired,
  modalLabel: string.isRequired,
  title: string,
  subtitle: string
};

export default Modal;
