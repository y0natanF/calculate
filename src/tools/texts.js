const texts = {
  intro: {
    main: '<mark>Er du en av de som vil tape mer enn halve inntekten</mark> din hvis du skulle bli ufør?',
    info: 'Storebrand kan hjelpe deg dekke tapet.',
    fwdButton: 'Dekk tapet'
  },
  salary: {
    main: '<mark>For å se hvor mye du burde dekke</mark>, trenger vi inntekten din.',
    fwdButton: 'Neste',
    backButton: 'Tilbake',
    label: 'Min årlige inntekt er',
    validation: 'Lønnen må være mellom 70 000 og 3 000 000',
    salary: {
      id: 'salary',
      label: 'Din årslønn',
      defaultValue: 560000,
      margin: 'normal'
    }
  },
  results: {
    main: '<mark>Du taper ca %% kr/mnd</mark> hvis du blir langvarig syk og ikke kan jobbe. Dette tilsvarer %% kr i året.',
    lossOnMinIncome: '<mark>Vi kan ikke beregne tapet ditt på grunn av noen kompliserte regler som gjelder.</mark> Du kan ringe til Storebrand, hvis du vil diskutere din situasjon.',
    lossOnMaxIncome: '<mark>Ditt tap er mer enn 1 million kroner.</mark>',
    backButton: 'Tilbake',
    fwdButton: 'Se pris på forsikring',
    calculationDescriptionLink: '* Slik har vi beregnet ditt tap',
    calculationDescriptionTitle: 'Slik har vi beregnet ditt tap',
    calculationDescriptionText: 'Vi har brukt lønnen du oppgav til å estimere hva du vil få fra nav dersom du blir syk og ikke kan jobbe. Vi har også gjort en enkel skatteberegning - både på lønnen din og på beløpet du vil få fra nav. Skatt har nemlig en god del å si for dette regnestykket.',
    infoDescriptionLink: '* Slik har vi beregnet uføresannsynlighet',
    infoDescriptionTitle: 'Slik har vi beregnet uføresannsynlighet',
    infoDescriptionText:
      'Ca. 20% av ansatte hos bedrifter som har uføreforsikring i Storebrand blir ufør mellom 35 år og pensjonsalder. Tallet for resten av Norges befolkning er høyere enn dette.',
    child: ' Når voksne blir ufør, er det ofte <mark> barna det går hardest utover.</mark>',
    bandaidHead: 'Visste du at <mark>1 av 5* blir helt eller delvis ufør</mark> før de går av med pensjon?'
  },

};

export default texts;
