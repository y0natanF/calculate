import React from 'react';

export const trimSalarySeparator = value => value.replace(/\s/g, '');

export const addSpaceSeparator = number => {
  const value = number.toString();
  if (value.length <= 3) return value;
  const chunks = [];
  let head = value;
  while (head.length > 3) {
    const tail = head.slice(-3);
    head = head.slice(0, -3);
    chunks.unshift(tail);
  }
  chunks.unshift(head);
  return chunks.join(' ');
};

const textMatcher = /<mark>(.*)<\/mark>/;

export const paragraphFormatter = (
  text,
  data = [],
  classes = '',
  placeholder = '%%'
) => {
  let input = text;
  data.forEach(d => {
    input = input.replace(placeholder, d);
  });
  const matchHighlighted = textMatcher.exec(input);
  let highlightedText = matchHighlighted ? matchHighlighted[1] : '';
  const normalText = input.replace(textMatcher, '[*]').split('[*]');
  const p1 = normalText[0] || '';
  const p2 = normalText[1] || '';
  return (
    <p className={`text ${classes}`}>
      {p1}
      {highlightedText && <span className="highlight">{highlightedText}</span>}
      {p2}
    </p>
  );
};
