import { trimSalarySeparator } from './helpers';
import { lossRange } from './recommendedValue';

import texts from './texts';
import {
  CLOSE_MODAL,
  NAVIGATE_RESULT,
  OPEN_INFO_DESCRIPTION,
  OPEN_RESULT_DESCRIPTION,
  SET_SALARY,
  SET_SALARY_ERROR,
} from './actions';

const lowestSalary = 350000;
const highestSalary = 2110000;


const calculateTotalLoss = (salary) => {
  let totalLoss = {
    lossValue: 0
  };

  const lossOnIncome = () => {
    let amount = lowestSalary;
    lossRange.forEach((range, index) => {
      (index < lossRange.length - 1 && salary >= range.revenue && salary < lossRange[index + 1].revenue) ?
        amount = range.value : null;
    });
    return amount;
  };

  (salary < lowestSalary) ? totalLoss.lossValue = lowestSalary :
    (salary > highestSalary) ? totalLoss.lossValue = highestSalary :
      totalLoss.lossValue = (lossOnIncome());
  return totalLoss;
};

const updateIfLower = (oldValue, newValue) =>
  oldValue > newValue ? oldValue : newValue;
const setResults = state => {
  const { salary } = state;
  const trimmedSalary = trimSalarySeparator(salary);
  const updateState = calculateTotalLoss(trimmedSalary);
  return {
    ...state,
    ...updateState,
    pageAuth: updateIfLower(state.pageAuth, 4)
  };
};

const initState = {
  salary: '',
  lowestSalary,
  highestSalary,
  salaryErrorText: '',
  salaryTouched: false,
  lossValue: 0,
  pageAuth: 0,
  showResultDescription: false,
  showInfoDescription: false,
  texts
};

const reducer = (state = initState, action) => {

  switch (action.type) {
  case SET_SALARY:
    return {
      ...state,
      ...action.payload,
      salaryTouched: true,
      pageAuth: 1,
      showResultDescription: false
    };
  case SET_SALARY_ERROR:
    return { ...state, ...action.payload, pageAuth: 1 };
  case NAVIGATE_RESULT:
    return setResults(state);
  case OPEN_RESULT_DESCRIPTION:
    return {
      ...state,
      showResultDescription: true,
      showInfoDescription: false
    };
  case OPEN_INFO_DESCRIPTION:
    return {
      ...state,
      showResultDescription: false,
      showInfoDescription: true
    };
  case CLOSE_MODAL:
    return {
      ...state,
      showResultDescription: false,
      showInfoDescription: false
    };
  default:
    return state;
  }
};

export default reducer;
