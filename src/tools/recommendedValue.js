export const lossRange = [
  { revenue: 350000, value: 7000 },
  { revenue: 380000, value: 8000 },
  { revenue: 410000, value: 9000 },
  { revenue: 460000, value: 10000 },
  { revenue: 500000, value: 10000 },
  { revenue: 540000, value: 11000 },
  { revenue: 580000, value: 12000 },
  { revenue: 600000, value: 13000 },
  { revenue: 610000, value: 14000 },
  { revenue: 630000, value: 15000 },
  { revenue: 650000, value: 16000 },
  { revenue: 660000, value: 17000 },
  { revenue: 680000, value: 17000 },
  { revenue: 700000, value: 18000 },
  { revenue: 710000, value: 19000 },
  { revenue: 730000, value: 20000 },
  { revenue: 750000, value: 21000 },
  { revenue: 770000, value: 22000 },
  { revenue: 780000, value: 23000 },
  { revenue: 800000, value: 24000 },
  { revenue: 820000, value: 24000 },
  { revenue: 830000, value: 25000 },
  { revenue: 850000, value: 26000 },
  { revenue: 870000, value: 27000 },
  { revenue: 880000, value: 28000 },
  { revenue: 900000, value: 29000 },
  { revenue: 920000, value: 30000 },
  { revenue: 930000, value: 31000 },
  { revenue: 950000, value: 31000 },
  { revenue: 970000, value: 32000 },
  { revenue: 990000, value: 33000 },
  { revenue: 1000000, value: 34000 },
  { revenue: 1020000, value: 35000 },
  { revenue: 1040000, value: 36000 },
  { revenue: 1060000, value: 37000 },
  { revenue: 1070000, value: 37000 },
  { revenue: 1090000, value: 38000 },
  { revenue: 1110000, value: 39000 },
  { revenue: 1130000, value: 40000 },
  { revenue: 1150000, value: 41000 },
  { revenue: 1160000, value: 42000 },
  { revenue: 1180000, value: 43000 },
  { revenue: 1200000, value: 44000 },
  { revenue: 1220000, value: 44000 },
  { revenue: 1240000, value: 45000 },
  { revenue: 1250000, value: 46000 },
  { revenue: 1270000, value: 47000 },
  { revenue: 1290000, value: 48000 },
  { revenue: 1310000, value: 49000 },
  { revenue: 1320000, value: 50000 },
  { revenue: 1340000, value: 51000 },
  { revenue: 1360000, value: 51000 },
  { revenue: 1380000, value: 52000 },
  { revenue: 1400000, value: 53000 },
  { revenue: 1410000, value: 54000 },
  { revenue: 1430000, value: 55000 },
  { revenue: 1450000, value: 56000 },
  { revenue: 1470000, value: 57000 },
  { revenue: 1490000, value: 58000 },
  { revenue: 1510000, value: 58000 },
  { revenue: 1520000, value: 59000 },
  { revenue: 1540000, value: 60000 },
  { revenue: 1560000, value: 61000 },
  { revenue: 1570000, value: 62000 },
  { revenue: 1590000, value: 63000 },
  { revenue: 1610000, value: 64000 },
  { revenue: 1630000, value: 65000 },
  { revenue: 1650000, value: 65000 },
  { revenue: 1660000, value: 66000 },
  { revenue: 1680000, value: 67000 },
  { revenue: 1700000, value: 68000 },
  { revenue: 1720000, value: 69000 },
  { revenue: 1730000, value: 70000 },
  { revenue: 1750000, value: 71000 },
  { revenue: 1770000, value: 71000 },
  { revenue: 1790000, value: 72000 },
  { revenue: 1810000, value: 73000 },
  { revenue: 1820000, value: 74000 },
  { revenue: 1840000, value: 75000 },
  { revenue: 1860000, value: 76000 },
  { revenue: 1880000, value: 77000 },
  { revenue: 1900000, value: 78000 },
  { revenue: 1910000, value: 78000 },
  { revenue: 1930000, value: 79000 },
  { revenue: 1950000, value: 80000 },
  { revenue: 1970000, value: 81000 },
  { revenue: 1980000, value: 82000 },
  { revenue: 2000000, value: 83000 },
  { revenue: 2020000, value: 84000 },
  { revenue: 2040000, value: 85000 },
  { revenue: 2060000, value: 85000 },
  { revenue: 2070000, value: 86000 },
  { revenue: 2090000, value: 87000 },
  { revenue: 2110000, value: 0 },
];
