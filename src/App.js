import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import { HashRouter as Router, Redirect, Route, Switch } from 'react-router-dom';


import Intro from 'pages/Intro';
import Results from 'pages/Result';


const theme = createMuiTheme({
  palette: {
    primary: { main: '#1e838f', dark: '#1e838f' },
    error: { main: '#BB1B18' }
  },
  typography: {
    fontSize: 14,
    fontFamily: '"Open Sans",Arial,ArialMT,sans-serif'
  }
});

const Main = ({ pageAuth }) => {
  return (
    <div className="insurance-container">
      <Router>
        <Switch>
          <Route exact path="/" component={Intro}/>
          {pageAuth > 0 && <Route path="/salary" component={Results}/>}
          {pageAuth > 1 && <Route path="/results" component={Error}/>}
          <Redirect to="/"/>
        </Switch>
      </Router>
    </div>
  );
};
const { number } = PropTypes;
Main.propTypes = {
  pageAuth: number
};


class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Main/>
      </MuiThemeProvider>
    );
  }
}

export default App;
