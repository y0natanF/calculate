# check-your-loss

> Frontend disability insurance value calculator prototype

## Develop

### Setup

```bash
$ npm install
```

### Run develop

```bash
$ npm start
```

Go to http://localhost:8080/

### Run tests

```bash
$ npm test
```

### Development strategy

We use a slightly modified variant of [Github Flow](http://scottchacon.com/2011/08/31/github-flow.html). The process is follows.

1.  To work on something new, create a branch off of `staging` with the JIRA issue number, prefixed with the type of task. Prefix can be `feature/` `bugfix/` or (if you for some reason need to create a hotfix), `hotfix/`. E.g. a branch name can be `feature/BI-1234`.
2.  Commit to that branch locally and regularly push your work to a branch with the same name on the server. Include the JIRA-number in each commit message, e.g. at the start. Try to follow [good commit practice](https://chris.beams.io/posts/git-commit/).
3.  When you are done with the task (see the Definition of Done below), it is ready for Quality Assurance (QA, i.e. code reivew). Then you can create a pull request to `staging`. Please use a descriptive title on your pull request, and include a description that explains the context of it.
4.  When the reviewer has approved the code, the branch will be merged into `t` for testing purposes.
5.  When the tester moves the ticket from JIRA column Testable to Ready to Release, the pull request will be merged to master (with --no-ff).
6.  Once it is merged to `master` and pushed to origin, it will be deployed as soon as possible to production. Anything in the master branch is deployable.

Branches **u** and **t** can be deleted anytime and recreated from the master branch. These branches represent their respective environment and feature branches are merged into them for testing purposes. Deleting any of these branches will disable the Bamboo build script for the branch you just deleted. So you will have to re-enable it.

#### Defintion of Done (DoD)

A task/item is completed and ready for QA when:

- The specification that is given in the JIRA-task is met
- There are no linting errors nor warnings. Running the app locally with `npm start` will give you all warnings and errors.
- The tests run OK. You run them with the command `npm test`

When these criteria are met, there can be created a pull request to `staging` for the task.

### Code style

Add linting rules in .eslintrc, but please discuss new additions with the team before you add them. There should be no linting warnings nor errors when one is creating a pull request.

### Architectural changes

Any change of the architecture, e.g. introducing new libraries, new coding styles or moving code to create a new structure in the project, should first be disucces with the team. Just bring the subject up in the Slack-channel.

## Build and Release

### Branches

TODO

### Build and deploy process

#### Automatic deploy

TODO

#### Manual deploy

1.  Build the production bundle

```bash
$ npm run build
```

2.  Compress the artifact

```bash
# the scripts in the deploy folder need to be made executable with `chmod +x <filename>` before using them for the first time.
$ cd ./deploy
$ ./zipartifact.sh
```

3.  Deploy the Snapshot or the Release artifact

```bash
# Remember to manually change the version in the script before launching it !!!
$ ./deploy-snapshot.sh [or] ./deploy-release.sh
```
